---
layout: post
title: "알고있지만 등장인물관계도 넷플릭스 원작웹툰 몇부작 등 총정리!"
toc: true
---

## 알고있지만 등장인물관계도 넷플릭스 원작웹툰 몇부작 등 총정리!

### 알고있지만 등장인물관계도 넷플릭스 원작웹툰 몇부작 등 총정리!

 2021.06.19~ 첫방송!
 jtbc 토요드라마 밤 11시
 

##### 알고있지만 몇부작?

 10부작
 

#### 알고있지만 넷플릭스 출시?

 네, 됩니다.
 

#### 알고있지만 인물관계도

 

 " 알고있지만, 반면 불구하고."
 

##### 알고있지만 등장인물

##### 한소희
 

 유나비 / 사랑은 못 믿어도 연애는 하고 싶은 여자
 ""뭔가 시작되려는 느낌은 착각이 아닐 거다.
넌 내가 나비라는 걸 알고 있잖아."

 

 뚜렷한 이목구비에 지적인 분위기,
왠지 차가워 보이는 인상에 말수도 적어 첫인상이 도도해 보인다는 소릴 듣지만,
의외로 배려심 깊고 털털한 성격이다.

어려서부터 네놈 앞가림 잘하는 조숙한 아이였고,

유독 미술에 소질을 보여 교사들과 또래 친구들의 주목도 많이 받았다.

무리 가난히 서방 경쟁률이 높은 미대의 조소과에 합격했으며,

입학 후에도 과 에이스로 활약 중이다.
 
그런 나비가 스무 살이 되어 최초 한계 연애는 좋게 말해 '의젓한' 연애였다.

하지만 사랑이라 믿었던 2년여 시간이, 임계 번에 무너져 내렸다.

그리고 다짐했다. 외당 목숨 같은 거 다시는 믿지 않겠다고.
하지만 그런 결심이 무색하게 나비에갠 진개 '운명적인' 만남이 찾아오는데
목 뒤에 나비 모양 문신을 한 남자,

펍 안 모두의 시선이 한 데 모일 만큼 잘생긴 남자 ‘재언’이 나타나 나비에게 다가온다.

 
나비는 한사코 마법에 걸린 듯 그에게서 눈을 뗄 수가 없었고,
그가 혹자 질아 복운 없는 꽃이어도, 나비는 도통 박재언을 외면할 수가 없다.

심지어 다른 여자와 키스하는 재언의 모습을 보고서도...
"난 사실 너랑 하고 싶었어."라는 인제 개소리를,
다른 여자의 립스틱이 여태껏 지워지지도 않은 입술로 다가오는 그를, 믿고 싶다.
그렇게 나비에겐 다시 기대가 생기고 그만큼 낙담 슬픔, 해소되지 않는 쓸쓸함 자괴감 허무함도 쌓여간다.

 

##### 송강
 

 박재언 / 연애는 성가셔도 썸은 타고 싶은 남자
 " 갈애 안할거면, 친하게 지내지도 못하나? "
 

 길에서 마주치면 누구나 테두리 번씩은 돌아보는 훤칠한 외모와 목소리. 

이렇게 잘생겼는데 배우도 모델도 아닌 조각하는 남자라니, 입학하자마자 스타가 되기에 충분한 조건이었다.
재능있는 신입생으로 내자 받지만, 사실은 엄청난 노력파로 독자 작업실에 틀어박혀 있는 시간을 즐긴다.
재언은 누구에게나 친절하고 유쾌하지만, 데이터 그 누구에게도 일정 소망 관심이 없다.

자신과 타인 사이에 완벽하게 선을 그어 놓고 제 속은 잘 드러내지 않는 스타일로,

관계에 있어 늘 한발 물러나 있기에 상대의 감정 파악이 빠르다.
다들 재언의 부드러운 언행에 기수 채지 못하지만, 유지 마음을 꿰[웹툰 추천](https://eli.kr/rank/free-webtoon)뚫어보는 찰나의 눈빛이 서늘하다.

 
예전부터 부 밖으로만 나가면, 사람들은 부모님을 쏙 빼닮아 잘생겼다며 그를 찬양했고
덕분에 자신의 외모가 호감이란 걸 일찍부터 인지해 그걸 적극 활용해왔다.

"너도 같이 즐겼잖아."
세상 가부 하찮은 게 인계 마음이고, 적시적소에 노상 이용하면 그뿐이라는 생각으로.

하지만 어느 날부턴가 혼자일 때면 떠오르는 여자가 생겼다, 유나비.
한 번 꼬셔볼까, 반쯤 장난스러운 마음으로 접근했는데… 자꾸만 나비에게 흥미가 생긴다.

그런 재언은 나비가 마음에 들면 들수록, 엉망진창으로 망가뜨려보고 싶다는 모순적인 감정을 느끼는데…
 

##### 채종협
 

 양도혁 / 짝사랑 말고 실지 연애를 해보고싶은 남자
 "너한테 다른 영장 생기거나 다른 이유 없어도,

난 아니라고 할 귀루 까진 포기 못할 것 같아"

태생적으로 긍정적이며, 순하다.

서울에 있는 대학에 진학하기 전까지 쭉 고향에서 자랐으며,

평생 국수집을 운영하신 할아버지의 영향으로 조리과에 진학했다.

몇 월광 전 전역했지만 대뜸 복학하지 않고 본가에 내려와 지내며 '국수집 손자'라는 요렇게 유튜브 채널을 간리 중이다.
10년 만에 재회한 첫사랑 나비를 본 순간, 도혁은 되처 사랑을 느낀다.

도혁은 나비와 요리도 하고, 술 마시며 밤새 수다도 떨고, 산책도 하며 둘만의 추억을 쌓는다.

그 과정에서 도혁은 나비가 누군가에게 깊은 상처를 받은 상태란 걸 낌새 채지만, 굳이 캐묻지 않는다.

나비의 상처를 헤집고 싶지 않기 때문이다.
자신과 함께하는 이 순간 온전히 즐겁고 행복할 수 있기를 바란다.

지금은 그것이 나비의 곁에 머물 명맥 있는 최선의 방법이니까…
 
그렇게 시간이 흐르고… 나비는 곧 다시 서울로 떠난다고 한다.

그래서 도혁은 직진을 택한다.

진심어린 고백에 일각 눈빛이 흔들리는 나비를 보며 작은 희망을 느끼는데…
그런데 이이 순간, 박재언이 나비를 찾아오고
 태연한 표정으로 도혁에게 악수를 청하는 재언을 보는 순간, 도혁은 알았다.

저 새끼가 나비를 뒤흔들고 상처 준 장본인이란 걸. 그리고 도혁은 보고 만다.

재언을 보는 순간 심장이 덜컥 내려앉는 듯한 표정의 나비를…. 도혁은 투지가 끓어오른다.
 

##### 이열음
 

 윤설아 / 재언의 중학교 동창이자 전 여자친구
 "예전에 사겼었어요. 근데 다시 만나고 있어요.

어제부터 새삼스레 사귀기로 했는데…"
 

 재언은 설아의 첫사랑이자, 첫 연인이었다.

 첫 키스도 첫 경험도 전면 재언이었다.

오랜 시간을 공유한 덕에,

설아는 재언의 친절한 미소 뒤에 감춰진 어두운 면을 잘 알고 있다.
2년 전, 미국으로 유학을 떠나게 되면서 재언과의 관계가 소원해졌다.

인정하고 싶지 않지만, 한국을 뜨게 된 여러 이유 중 재언도 있었다.

그에게 한결 희망 휘둘리고 싶지 않다는 마음.

하지만 결국 재언을 잊을 수 없었고, 방학을 맞아 큰맘 먹고 한국에 들어왔다.

늘 집을 벗어나고 싶어 하는 재언에게, 함께 한국을 떠나자고 제안하려 했던 것.
재언은 여느 때처럼 연락을 하자마자 한달음에 달려왔다.

그런데, 뭔가 달라졌다.

늘 텅 비어있던 재언의 두 눈이 누군가를 원하고 있었다.

이 세상 누구보다도 재언에 대해 잘 알고 있다고 생각했는데…

설아는 처음 보는 재언의 모습에 안도하면서도, 한편으로 서운하다.
 

#####  양혜지
 

 오빛나 / 나비의 절친, 학생회
 

 모르는 게 없는 조소과의 정보통.

 눈치가 빨라 하나를 보면 열을 알고, 온갖 가십거리와 비밀에 24시간 촉을 곤두세운다.

구미가 당기는 일 앞에서는 우정도 의리도 사라지는 스타일.

철저히 본능에 충실, 자유로운 만남을 추구하지만 가급적 학원 눈치 연애는 지양하려는 편.
다른 사람들의 가십은 재밌지만, 본인이 댁 주인공이 되는 건 싫기 때문이다.

그런데 절친한 과 동기 규현이 이런 빛나의 철칙을 깨부수려 한다.
 

 남규현 / 빛나의 절친, 학생회
 과 동기이자 같은 학생회인 빛나와 절친한 사이.
 

 훈훈한 외모에 밝고 둥글둥글한 성격으로 주위에 친구가 많다.
연애에 있어서는 보수적인 면이 있어, 개방적인 연애관을 가진 빛나와 자주 투닥거린다.

이처럼 친구로는 좋지만 이성으로 서는 성격도, 연애에 대한 생각도, 이이 뭣 한쪽 맞는게 없는데..
어느 날부턴가 자꾸만 빛나에게 눈이 간다.
 

 윤솔 / 나비의 절친, 학생회

교수들이 주목하는 조소과 에이스로,

매사 성실하고 신중한 편이다.
인간관계에 있어서도 늘 일정 거리를 지키는 담백한 타입.
예외가 있다면, 중고등학교 동창이자 같은 과 지완 뿐이다.
 

 서지완 / 학생회

솔과는 극과 극일 정도로 감성적이며 솔직한 기분파.

학창시절 내내 단짝이었던 솔을 따라 힘들게 조소과에 들어왔지만,
3학년이 된 지금까지도 미술에는 크게 흥미를 느끼지 못한다.
 

 안경준 / 조소과 조교

나비와 재언의 조소과 선배이자 대학원생.

민영과 함께 과사 조교로 근무 중이다.
 

 인싸력 만렙으로 후배들은 마땅히 근순 사람들을 챙기느라 일을 늘리는 타입.

때문에 과사 사무실은 경준을 찾는 학생들로 항상 북적인다.
민영에게 오지라퍼라는 핀잔을 들으면서도 마냥 증겁기만 한 경준.
그만큼 주위에 늘 사람이 많다.
 

##### '드라마' 카테고리의 다른 글

### 태그

### '드라마' Related Articles
